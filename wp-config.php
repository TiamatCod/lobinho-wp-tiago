<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yt1TkJO46+BKxrkMLNXVUkzl4i5/eUn1XAW5+68Hzwri7K/EQzIJ/vAN3CT99vFVkJEoTTVq27TbTuA9Z7uWUQ==');
define('SECURE_AUTH_KEY',  'mq/LKGt4Z5zm8OqiOcE6rPy9+RKoYq5a6sDkK6jS0H3mDRpU8Ungz9jErVF5FM9ssNXh2xKfJyicb74+su4Dzg==');
define('LOGGED_IN_KEY',    'Ud+NaouA/e6DEN2PnzZiFk0nXqYd7I25z2zlvF22UyW9QOqS1Woh8pxR21whDslA6yjqaCCbN21PBwZgZX5D6Q==');
define('NONCE_KEY',        'ElQ1jix7exbH4W++jS5KVmnUuh1VnZgStAxRdRrwUmE/c56CD5U0u8VW4xEyC4G2atiGNcFsocrnRV+dHwvQzA==');
define('AUTH_SALT',        'KiHBlvddtL8e0/R5SLpDTl+k7+wk3mj+NOYrgUetGUJEbSo5BvcA3W1j1KinkStOahYMBWyqkMVuemIyYWIU6w==');
define('SECURE_AUTH_SALT', 'qkGDtXYjQaQ1WDIlmIUjZg0S3e0v/s6+iFLDoLg2GGSUzVDTDf2VAAUso2r0gfcGS/Lj6G6uNBQe7dcen2DK4g==');
define('LOGGED_IN_SALT',   '+J6YzncXxSJtbF+1B+Bjc02hZjpG0fO+Gh/xfRHTOr9n7iqHsnHkfb62Ekf7UfH8rcOz51eaIScX2Dq0W1N17w==');
define('NONCE_SALT',       'zl6Gdo3nLwkjr4xndBjxsHYsxFePDcXOmKy6FnoxWafb9sWx4lP+DlyzO/XwTdt2hlyAJu84vrVtKCumrSbhlg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
